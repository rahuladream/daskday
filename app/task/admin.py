from django.contrib import admin
from .models import *
# Register your models here.

class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_by', 'priority', 'is_active']
admin.site.register(Task, TaskAdmin)