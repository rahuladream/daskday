from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
# Create your models here.

class Task(models.Model):
    
    title = models.CharField(max_length=50)
    created_at = models.DateField(auto_now_add=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    note = models.TextField(blank=True)
    priority = models.PositiveIntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "task"
        verbose_name_plural = "tasks"
        ordering = ["priority"]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("task_detail", kwargs={"pk": self.pk})