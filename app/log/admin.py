from django.contrib import admin
from .models import *
# Register your models here.

class LogAdmin(admin.ModelAdmin):
    list_display = ['task', 'created_date', 'completed']
admin.site.register(Log, LogAdmin)