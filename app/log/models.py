from django.db import models
import datetime
from app.task.models import Task
from django.contrib.auth.models import User
# Create your models here.

class Log(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    created_date = models.DateField(auto_now_add=True)
    due_date = models.DateField(blank=True, null=True)
    completed = models.BooleanField(default=False)
    notes = models.TextField(blank=False)
    completed_date = models.DateField(blank=True, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def overdue_status(self):
        "Returns whether the tasks due date has been passed or not."
        if self.due_date and datetime.date.today() > self.due_date:
            return True

        def __str__(self):
            return self.task.title
        
        def save(self, **kwargs):
            "If task is being marked complete, set the completed date"
            if self.completed:
                self.completed_date = datetime.datetime.now()
            super(Log, self).save()
        